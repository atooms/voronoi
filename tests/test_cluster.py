#!/usr/bin/env python

import unittest
from atooms.voronoi.helpers import cluster_analysis

class TestCluster(unittest.TestCase):

    def setUp(self):
        pass

    def test_simple_voronoi(self):
        inp = [[(0, 2, 8), (0, 2, 8, 1)],
               [(0, 2, 3, 4), (0, 2, 3, 4), (0, 2, 1)],
               [(0, 2, 8), (0, 3, 6)]]
        ref = [set([(0, 2, 8, 1), (0, 2, 8), (0, 3, 6)]),
               set([(0, 2, 3, 4), (0, 2, 1)])]
        clusters = cluster_analysis(inp)
        self.assertEqual(clusters, ref)


if __name__ == '__main__':
    unittest.main(verbosity=0)
