#!/usr/bin/env python

import unittest
from atooms.voronoi.helpers import lifetime

class TestCluster(unittest.TestCase):

    def setUp(self):
        pass

    def test_lifetime_short(self):
        steps = [1, 2, 3, 4, 5]
        cluster = [dict(value=(1, 2), tag="b"),
                   dict(value=(1, 2), tag="b"),
                   dict(value=(1, 2), tag="a"),
                   dict(value=(2, 3), tag="a"),
                   dict(value=(2, 3), tag="a")
                   ]
        a = lifetime(cluster, steps=steps)
        self.assertEqual(a, [1, 0, 1])

    def test_lifetime_long_tag(self):
        cluster = [dict(value=(1, 2), tag="b"),
                   dict(value=(1, 2), tag="b"),
                   dict(value=(1, 2), tag="a"),
                   dict(value=(1, 2), tag="a"),
                   dict(value=(1, 2), tag="b"),
                   dict(value=(1, 2), tag="a"),
                   dict(value=(2, 3), tag="a"),
                   dict(value=(2, 3), tag="a")
                   ]
        steps = range(len(cluster))

        def f(p0, p):
            result = False
            if p0['tag'] == p['tag']:
                if sorted(p0['value']) == sorted(p['value']):
                    result = True
            return result
        a = lifetime(cluster, steps=steps, identity_fct=f)
        self.assertEqual(a, [4, 0, 1])

    def test_lifetime_long_tag_mask(self):
        cluster = [dict(value=(1, 2), tag="b"),
                   dict(value=(1, 2), tag="b"),
                   dict(value=(1, 2), tag="a"),
                   dict(value=(1, 2), tag="a"),
                   dict(value=(1, 2), tag="b"),
                   dict(value=(1, 2), tag="a"),
                   dict(value=(2, 3), tag="a"),
                   dict(value=(2, 3), tag="a")
                   ]
        steps = range(len(cluster))

        def f(p0, p):
            result = False
            if p0['tag'] == p['tag']:
                if sorted(p0['value']) == sorted(p['value']):
                    result = True
            return result
        times = lifetime(cluster, steps=steps, identity_fct=f, mask=[p['tag'] == 'a' for p in cluster])
        self.assertEqual(times, [3, 1])

    def test_read(self):
        from atooms import voronoi
        t = voronoi.TrajectoryVoronoi('data/wahn.voronoi.xyz')
        v0 = []
        for s in t:
            v = s.voronoi
            v0.append(v[-1].signature)

        def f_match(p0, p):
            if p0.signature == p.signature and \
               sorted(p0.neighbors) == sorted(p.neighbors):
                return True
            else:
                return False

        times = []
        for i in [-1]:
            def f_attr(t, ti):
                return t[ti].voronoi[i]
            _ = lifetime(t, identity_fct=f_match, attribute_fct=f_attr,
                         mask=[s.voronoi[-1].signature == (0, 0, 12) for s in t])
            self.assertEqual([10], _)


if __name__ == '__main__':
    unittest.main(verbosity=0)
