#!/usr/bin/env python

import os
from setuptools import setup

with open('atooms/voronoi/_version.py') as f:
    exec(f.read())

with open('README.md') as f:
    readme = f.read()

setup(name='atooms-voronoi',
      version=__version__,
      description='Voronoi wrapper to voro++ and analysis tools',
      long_description=readme,
      long_description_content_type="text/markdown",
      author='Daniele Coslovich',
      author_email='dcoslovich@umontpellier.fr',
      url='https://framagit.org/atooms/voronoi',
      packages=['atooms', 'atooms/voronoi'],
      license='GPLv3',
      install_requires=['atooms>=3.0'],
      scripts=['bin/voro.py', 'bin/voropp.py'],
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python',
      ]
     )
