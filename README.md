Sample
==================

Sample package

Quick start
-----------

```python
import sample
```

Installation
------------
The easiest way to install sample is with pip
```
pip install sample
```

Alternately, you can clone the code repository and install from source
```
git clone git@framagit.org:coslo/sample.git
cd sample
make install
```

Features
--------

- Feature 1
- Feature 2

Contributing
------------
Contributions to the project are welcome. If you wish to contribute, check out [these guidelines](https://framagit.org/atooms/atooms/-/blob/master/CONTRIBUTING.md).

Authors
-------
Daniele Coslovich: https://www.units.it/daniele.coslovich/
