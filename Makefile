PROJECT = atooms/voronoi
PACKAGE = atooms.voronoi

.PHONY: version test install docs coverage pep8 clean

all: install

version:

test:
	python -m unittest discover -s tests

install: version
	python setup.py install

docs:

coverage:
	coverage run --source $(PACKAGE) -m unittest discover -s tests
	coverage report

pep8:
	autopep8 -r -i $(PROJECT)
	autopep8 -r -i tests
	flake8 $(PROJECT)

clean:
	find $(PROJECT) tests -name '*.pyc' -exec rm '{}' +
	find $(PROJECT) tests -name '__pycache__' -exec rm -r '{}' +
	rm -rf build/ dist/


